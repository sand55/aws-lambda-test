'use strict'

// Rate または Cron を使用したスケジュール式
// https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/tutorial-scheduled-events-schedule-expressions.html
//

const AWS = require('aws-sdk');

const SES = new AWS.SES({
  apiVersion: '2010-12-01',
  region: 'us-west-2'
})

exports.handler = (event, context, callback) => {

// --- var ses = new AWS.SES();

  var params = {
    Destination: {
      ToAddresses: [ 'sand.mypress@gmail.com' ]
    },
    Message: {
      Body: {
        Text: {
          Data: 'こんにちは SES',
          Charset: 'utf-8'
        }
      },
      Subject: {
        Data: 'こんにちは',
        Charset: 'utf-8'
      }
    },
    Source: 'melopachi367@gmail.com' // From
  };


  SES.sendEmail(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
  });
}
